import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.Move;

public class Columbus {
    byte grid[][] = new byte[200][200];
    Move movement;
    int x = 100;
    int y = 100;
    Compass rot = new Compass(1);
    UltrasonicSensor us = new UltrasonicSensor(SensorPort.S1);
    DifferentialPilot pilot = new DifferentialPilot(2.1f, 2.1f, Motor.A, Motor.B, true);
    public static void main(String[] args) {
        Columbus c = new Columbus();
        for(int i = 0; i < 200; i++) {
            for(int j = 0; j < 200; j++) {
                c.grid[i][j] = 0;
            }
        }

        int rDir;
        LCD.drawString("Enterknopf", 0, 0);
        LCD.drawString("um zu starten", 0, 1);
        LCD.drawString("...", 0, 2);
        while (true) {
            if (Button.ENTER.isDown()) {
                break;
            }
        }
        while (!Button.ESCAPE.isDown()) {
            rDir = c.getRandomDir();
            c.adjustDir(rDir);
            c.move();
            c.setLCD();
        }
    }

    public void turnLeft() {
        rot.turnLeft();
        pilot.rotate(-185);
    }

    public void turnRight() {
        rot.turnRight();
        pilot.rotate(185);
    }

    public boolean detObstacle() {
        if (us.getDistance() <= 30) {
            return true;
        }
        return false;
    }

    public void move() {
        pilot.travel(-30,true);
        setGrid();
        while(pilot.isMoving()) {
            if(detObstacle()) {
                grid[x][y] = 2;
                movement = pilot.getMovement();
                pilot.stop();
                pilot.travel(-movement.getDistanceTraveled());
                break;
            }
        }

        if(grid[x][y] == 0) {
            grid[x][y] = 1;
        }
    }

    public void adjustDir(int dir) {
        while (rot.getDirectionInt() != dir) {
            turnLeft();
        }
    }

    public void setGrid() {
        switch (rot.getDirection()) {
        case "north":
            x += 1;
            break;
        case "south":
            x -= 1;
            break;
        case "west":
            y += 1;
            break;
        case "east":
            y -= 1;
            break;
        default:
        }
    }

    public void setLCD() {
        LCD.clearDisplay();
        for(int i = -3; i < 4; i++) {
            for(int j = -5; j < 6; j++) {
                if(i == 0 && j == 0) {
                    LCD.drawChar('o', 6, 4);
                    continue;
                }
                switch(grid[x+j][y+i]){
                case 0:
                    break;
                case 1:
                    LCD.drawChar('.', i+3, j+5);
                    break;
                case 2:
                    LCD.drawChar('#', i+3, j+5);
                    break;
                default:
                    break;
                }
            }
        }
    }

    public int getRandomDir() {
        double r = Math.random();

        if (r > 0 && r < 0.26) {
            return 0;
        } else if (r > 0.26 && r < 0.51) {
            return 1;
        } else if (r > 0.51 && r < 0.76) {
            return 2;
        } else {
            return 3;
        }
    }
}
