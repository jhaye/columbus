
public class Compass {
	
	private int rotation;
	
	public Compass(int r) {
		this.rotation = r;
	} 
	
	public void turnLeft() {
		if(rotation != 0) {
			rotation -= 1;
		} else {
			rotation = 3;
		}
	}
	
	public void turnRight() {
		if(rotation != 3) {
			rotation += 1;
		} else {
			rotation = 0;
		}
	}
	
	public String getDirection() {
		
		String dir;
		
		switch(rotation){
			case 0: 
				dir = "west";
				break;
			case 1:
				dir = "north";
				break;
			case 2:
				dir = "east";
				break;
			case 3:
				dir = "south";
				break;
			default:
				dir = null;
		}
		
		return dir;
	}
	
	public int getDirectionInt() {
		
		int dir;
		
		switch(rotation){
			case 0: 
				dir = 0;
				break;
			case 1:
				dir = 1;
				break;
			case 2:
				dir = 2;
				break;
			case 3:
				dir = 3;
				break;
			default:
				dir = 4;
		}
		
		return dir;
	}
	
}
